<?php
/**
 * Instagram Feed
 */
	$custom_query_args_posts = array( 'id' => $instagram_feed_id );
	$custom_query = new WP_Query( $custom_query_args_posts ); ?>
	<div class="insta-container">
		<div class="row">
		<div class="col-xs-12 text-center ifg-post-separator"><p class="custom-link"><i class="fa fa-th"></i> <?php _e('POSTS', IFGP_TXTDM); ?></p></div>
		</div>
		<!-- dedicated container or MasonGram -->
		<div class="gallery-css" id="awl-instagram-gallery"></div>
	</div>
	<script>
	jQuery(document).ready(function (){
		var access_token = '<?php echo $instagram_acces_token; ?>'; // saved by user
		var $username = 'https://api.instagram.com/v1/users/self/?access_token=<?php echo $instagram_acces_token; ?>';
		var authorization = 'https://api.instagram.com/oauth/authorize/?client_id=<?php echo $instagram_client_id; ?>&redirect_uri=' + window.location + '&response_type=token&scope=public_content';

		jQuery('#awl-instagram-gallery').masongram({
			access_token: access_token,
			count: 5,
			size: 'low_resolution',
			caption: '<div class="text-center"><a tabindex="-1" class="" href="{link}" target="_new" data-if="{link}"><i class="fa fa-instagram ifg-image-icons"></i></a></div>'
		});
	});
	</script>