<?php
/**
 * Instagram Feed  Shortcode
 *
 * @access    public
 * @since     3.0
 *
 * @return    Create Fontend  Output
 */
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
add_shortcode('IFG', 'wp_instagram_feed_shortcode');
function wp_instagram_feed_shortcode($atts) {
	ob_start();
	
	//js
	//wp_enqueue_script('jquery');
	wp_enqueue_script('ifgp-bootstrap-js', IFGP_PLUGIN_URL .'js/bootstrap.js', array('jquery'), '' , true);
	wp_enqueue_script('ifgp-imagesloaded-pkgd-js', IFGP_PLUGIN_URL .'js/imagesloaded.pkgd.js', array('jquery'), '' , true);
	wp_enqueue_script('ifgp-isotope-js', IFGP_PLUGIN_URL .'js/isotope.pkgd.js', array('jquery'), '', false);
	wp_enqueue_script('ifgp-masongram-min-js', IFGP_PLUGIN_URL .'js/ifgp-masongram.min.js', array('jquery'), '', false);
	
	// awp custom bootstrap css
	wp_enqueue_style('ifgp-bootstrap-css', IFGP_PLUGIN_URL .'css/ifgp-bootstrap.css');
	wp_enqueue_style('ifgp-font-awesome-css', IFGP_PLUGIN_URL . 'css/font-awesome-4.min.css' );
	wp_enqueue_style('ifgp-masongram-css', IFGP_PLUGIN_URL . 'css/ifgp-masongram.css' );
	//Random id generate
	$instagram_feed_id = rand(1,10000000);

	//Access Token
	if(isset($atts['instagram_acces_token'])) $instagram_acces_token = $atts['instagram_acces_token']; else $instagram_acces_token = "";
	$instagram_client_id = substr($instagram_acces_token, 11);
	//Fetch Profile info
	$instagram_user_data = wp_remote_get("https://api.instagram.com/v1/users/self/?access_token=$instagram_acces_token");
	
	if(isset($atts['custom_css'])) $custom_css = $atts['custom_css']; else $custom_css = "";

	?>
	<!-- CSS Part Start From Here-->
	<style>
	<?php echo $custom_css; ?>
	.ifg-image-icons {
		color: #FFF;
	}
	.ifg-image-icons:hover {
		color: #FF0000;
	}
	</style>
	
	<?php
	//Include code file
	require('output.php');
	return ob_get_clean();
}
?>