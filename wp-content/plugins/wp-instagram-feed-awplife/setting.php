<?php
// CSS
wp_enqueue_style( 'ifgp-styles-css', IFGP_PLUGIN_URL . 'css/styles.css' );
wp_enqueue_style( 'ifgp-bootstrap-css', IFGP_PLUGIN_URL . 'css/ifgp-bootstrap.css' );
wp_enqueue_style( 'ifgp-font-awesome-4-min-css', IFGP_PLUGIN_URL . 'css/font-awesome-4.min.css' );
wp_enqueue_style( 'ifgp-hover-css', IFGP_PLUGIN_URL . 'css/hover.css' );
wp_enqueue_style( 'ifgp-settings-css', IFGP_PLUGIN_URL . 'css/ifgp-settings.css' );

//js
//wp_enqueue_script('jquery');
wp_enqueue_script( 'ifgp-bootstrap-js', IFGP_PLUGIN_URL  . 'js/bootstrap.js', array( 'jquery' ), '', true  );
wp_enqueue_script( 'ifgp-color-picker-js', plugins_url('js/ifgp-color-picker.js', __FILE__ ), array( 'wp-color-picker' ), false, true );

//uploader
wp_enqueue_media();
wp_enqueue_script('thickbox');
wp_enqueue_script('em-image-upload');
wp_enqueue_style('thickbox');
?>
<style>
.panel-info {
    border-color: #F5F5F5 !important;
}
</style>
<div class="panel panel-info" style="margin-top:20px; margin-bottom:10px;">
	<div class="panel-heading text-center">
		<h3 class="panel-title"><?php _e('Instagram Feed Settings', IFGP_TXTDM); ?></h3>
	</div>
	<div class="panel-body " style="padding-top:20px" id="BlogFilter-SettingsPags">
		<div class="module-wrapper masonry-item col-lg-12 col-md-12 col-sm-12 col-xs-12" style="">
			<section class="module module-headings">
				<div class="module-inner">
					<div class="module-content collapse in" id="content-1">
						<div class="module-content-inner">
							<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
								<p><b><?php _e('Instagram Access Token', IFGP_TXTDM); ?></b></p>
							</div>
							<div class="col-lg-8 col-md-8 col-sm-6 col-xs-6">
								<input type="text" id="instagram_acces_token" name="instagram_acces_token" placeholder="<?php _e('Your Instagram Access Token', IFGP_TXTDM); ?>" value="" style="width: -webkit-fill-available;" >
								<p>
								<?php _e('Example Token:', IFGP_TXTDM); ?> 2961809551.1677ed0.6840a7941ef945bf93cbabf2c4e38b1e<br>
								<?php _e('Generate & Copy Your Instagram Access Token', IFGP_TXTDM); ?>: <a href="https://awplife.com/instagram-access-token-generator/" target="_new"><?php _e('Automatically Generate', IFGP_TXTDM); ?></a><br>
								<?php _e('Generate & Copy Your Instagram Access Token', IFGP_TXTDM); ?>: <a href=" https://awplife.com/get-instagram-access-token/" target="_new"><?php _e('Manually Generate', IFGP_TXTDM); ?></a>
								</p>
							</div>
						</div>
						<div class="module-content-inner title_setings">
							<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
								<p><b><?php _e('Custom CSS', IFGP_TXTDM); ?></b></p>
							</div>
							<div style="margin-bottom:3%;" class="col-lg-8 col-md-8 col-sm-6 col-xs-6 ">
								<textarea id="custom_css" name="custom_css" class="col-md-12" rows="5" placeholder="<?php _e('Use custom CSS code without style tag.', IFGP_TXTDM); ?>" ></textarea>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>
</div>

<div class="panel panel-info igp_pannel_bottom">
	<div class="panel-body eva-bottom-panel">
		<div class="col-md-6 text-left">
			<div class="eva_option_head">
			<h3 class="igp_footer_title"><?php _e('Instagram Feed', IFGP_TXTDM); ?> <p style="display:inline;"><?php _e('Version', IFGP_TXTDM); ?> - <?php echo IFGP_PLUGIN_VER; ?><p></h3>
			</div>
		</div>
		<div class="col-md-6 text-right">
			<div class="eva_option_head">
			<button type="button" onclick="IgpGetShortcode();" class="igp_button button_1"><?php _e('[ Generate Sortcode ]', IFGP_TXTDM); ?></button>
			</div>
		</div>
	</div>
	
</div>

<div class="loader" style="display:none;"></div>

<div class="modal" id="modal-show-shortcode" tabindex="-1" role="dialog" aria-labelledby="modal-new-short-code-label">
	<div class="modal-dialog" role="document" id="inner-modal">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="modal-new-ticket-label"><?php _e('Instagram Feed Shortcode', IFGP_TXTDM); ?></h4>
			</div>
			<div id="" class="modal-body text-center">
				<textarea id="awl-shortcode" readonly rows="13" cols="120" style="width: 468px;">
				</textarea>
				<div id="" class="center-block text-center">
					<button type="button" class="igp_button button_1" data-toggle="tooltip" title="Copied" onclick="CopyShortcode()" ><i class="fa fa-copy" aria-hidden="true"></i> <?php _e('Copy Sortcode', IFGP_TXTDM); ?></button>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
function IgpGetShortcode() {
	
	var shortcode = '[IFG';
	
	var instagram_acces_token = jQuery("#instagram_acces_token").val();
	if(instagram_acces_token){
		shortcode = shortcode + ' instagram_acces_token="' + instagram_acces_token + '"';
	} else {
		shortcode = shortcode + ' instagram_acces_token="' + '2961809551.1677ed0.c61a67160cd647d4b537305ceb150ea4' + '"';
	}
	
	var custom_css = jQuery("#custom_css").val();
	if(custom_css){
		shortcode = shortcode + ' custom_css="' + custom_css + '"';
	} else {
		shortcode = shortcode + ' custom_css=""';
	}	
	
	
	shortcode = shortcode + ' ]';
	
	jQuery('#awl-shortcode').text(shortcode);
	jQuery('#modal-show-shortcode').modal('show');
}

function CopyShortcode() {
  var copyText = document.getElementById("awl-shortcode");
  copyText.select();
  document.execCommand("copy");
}
</script>