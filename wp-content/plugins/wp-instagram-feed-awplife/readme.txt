﻿=== Social Media Feed ===
Contributors: awordpresslife
Donate link: https://paypal.me/awplife
Tags: instagram feed, instagram gallery, instagram, feeds, instagram post
Requires at least: 4.0
Tested up to: 5.5.3
Stable tag: 1.1.4
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html


== Description ==

Social Media Feed plugin is a clean and beautiful plugin that helps you to display your Instagram photo album on your website or blog.

It is fully responsive and can work with all kind of devices like PC, mobile and tablet. Social Media Feed Plugin uses access token method to access your Insta galleries and display it on your website in very beautiful grid arrangement.

It can also show images/photos in masonry arrangement to make the album stunning even when you have set of different image sizes.

Social Media Feed plugin is user friendly and easy to use because it provides you option to generate shortcode of your gallery.

You can use these short codes anywhere and multiple times according to your requirement. Plugin is customizable and also comes with custom CSS option that allows you to tweak it to fit-in your needs. With the help of Social Media Feed plugin, you can import multiple galleries from different Insta accounts to your website or blog.

There will be link with every image you import, this link will take you to the source image on Instagram. It doesn’t matter how many images you have in your Instagram account, this plugin can handle all beautifully and load images one by one on page scroll.



https://youtu.be/Vo9amiqRBhM


Instagram Feed plugin import your Instagram post and display it on your website like a masonry layout. This gallery plugin is very simple so you very easily use this Social Media Feed plugin can do.

Social Media Feed plugin is a gallery plugin this plugin show Instagram on your websites like a gallery and every image link to click redirect your Instagram.


**Premium Features**

* Instagram Access Token
* Image Load Count
* Image Size
* Bootstrap 3.3.6 Based
* Profile
* Profile Posts
* Profile Followers
* Profile Followings
* Links
* Custom CSS
* Use via short-code.


**Check Plugin Demo		- <a href="https://awplife.com/demo/instagram-feed-gallery-premium/">Click Here</a>**

**Upgrade Premium		- <a href="https://awplife.com/wordpress-plugins/instagram-feed-gallery-premium/">Click Here</a>**


== Screenshots ==

1. Instagram Feed Preview 1
2. Instagram Feed Preview 2
3. Instagram Feed Preview 3


= How to use plugin? =

Download & install the plugin into your site. After successful installation of the plugin go to plugins "DOC" menu for help and instructions.


 == Installation ==

Install Instagram Feed either via the WordPress.org plugin directory or by uploading the files to your server.

After activating Instagram Feed plugin, go to plugin menu.

Login into WordPress admin dashboard. Go to menu: Instagram Feed --> Instagram Feed

Create Instagram Feed and save.

Copy shortcode and paste shortcode into any Page / Post. And view page for Instagram Feed output.

That's it. You're ready to go!

== Frequently Asked Questions ==

Have any queries?

Please post your question on plugin support forum

https://wordpress.org/support/plugin/wp-instagram-feed-awplife/


== Changelog ==

= 1.1.4 =
* Enhancements: Yes, checked for WordPress 5.5.3

= 1.1.3 =
* Enhancements: Yes, checked for WordPress 5.5.1

= 1.1.2 =
* Enhancements: Yes, checked for WordPress 5.4
* Enhancements: New UI added

= 1.1.1 =
* Added Jquery load function in header.

=1.1.0 =
* Enhancements: Yes, checked for WordPress 5.3.2

=1.0.10 =
* Enhancements: Yes, checked for WordPress 5.2.3

=1.0.9 =
* Enhancements: Yes, checked for WordPress 5.2.3

=1.0.8 =
* Enhancements: Yes, checked for WordPress 5.2.2
* Bug Fix: Fixed.
* Additional changes: banner image and icon change.

=1.0.7 =

* Enhancements: Yes, checked for WordPress 5.2.2
* Bug Fix: Fixed.
* Additional changes: banner image and icon change.

=1.0.6 =

* Enhancements: Yes, checked for WordPress 5.2.2
* Bug Fix: Fixed.
* Additional changes: None.

=1.0.5 =

* Enhancements: Yes, checked for WordPress 5.2.2
* Bug Fix: Fixed.
* Additional changes: None.

=1.0.4 =

* Enhancements: Yes, checked for WordPress 5.2.2
* Bug Fix: Fixed.
* Additional changes: None.

=1.0.3 =

* Enhancements: Yes, checked for WordPress 5.2.1
* Bug Fix: Fixed.
* Additional changes: None.

=1.0.2 =

* Enhancements: Yes, checked for WordPress 5.2.1
* Bug Fix: Fixed.
* Additional changes: None.

=1.0.1 =

* Enhancements: Yes, checked for WordPress 5.1.1
* Bug Fix: Fixed.
* Additional changes: None.

=1.0.0 =

* Enhancements: Yes, checked for WordPress 5.1.1
* Bug Fix: Fixed.
* Additional changes: None.

== Upgrade Notice ==
This is an initial release. Start with version 1.0.0 and share your feedback <a href="https://wordpress.org/support/plugin/wp-instagram-feed-awplife/reviews/">here</a>.