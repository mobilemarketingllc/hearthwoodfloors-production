<?php

/*
Template Name: No Header/Footer
Template Post Type: post, page
*/


get_header();
global $wpdb;
?>

<div class="fl-content-full container">
	<div class="row">
		<div class="fl-content col-md-12">
		<?php 
	 
		/*
		// template for inserting the shop_order posts
$post_sql_template = "INSERT INTO `wp_posts` (
            `post_author`,
            `post_date`,
            `post_date_gmt`,
            `post_content`,
            `post_title`,
            `post_excerpt`,
            `post_status`,
            `comment_status`,
            `ping_status`,
            `post_password`,
            `post_name`,
            `to_ping`,
            `pinged`,
            `post_modified`,
            `post_modified_gmt`,
            `post_content_filtered`,
            `post_parent`,
            `guid`,
            `menu_order`,
            `post_type`,
            `post_mime_type`,
            `comment_count`
        ) VALUES (
            %s, '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', %s, '%s', %s, '%s', '%s', %s);";

// template for inserting the order_items
$item_sql_template = "INSERT INTO `wp_woocommerce_order_items` (`order_item_name`, `order_item_type`, `order_id`) VALUES ('%s', '%s', '%s')";

// If you want to remove all orders in the target db, and replace with those from source, uncomment this block.
// I don't usually do this as leaving it commented, orders common to both systems will be skipped and so
// retain their original ids

// $sql = "DELETE FROM wp_woocommerce_order_itemmeta";
// mysqli_query($new_conn, $sql);
// $sql = "DELETE FROM wp_woocommerce_order_items";
// mysqli_query($new_conn, $sql);
// $sql = "DELETE FROM wp_posts WHERE post_type = 'shop_order'";
// mysqli_query($new_conn, $sql);

$sql = "SELECT * FROM wp_posts_1 WHERE post_type = 'shop_order' ";
$duplicates = $wpdb->get_results($sql,ARRAY_A);	
//print_r($duplicates);exit;
//$result_first = $wpdb->query($sql);

foreach($duplicates as $row)
{

$old_id = $row['ID'];
    // check to see if a shop_order post with this id exists - if so, skip
    // (see note on deleting above - this won't happen if that block is uncommented)
	 

	
    // add the post
    $post_sql = sprintf($post_sql_template,
        $row['post_author'],
        $row['post_date'],
        $row['post_date_gmt'],
        $row['post_content'],
        $row['post_title'],
        $row['post_excerpt'],
        $row['post_status'],
        $row['comment_status'],
        $row['ping_status'],
        $row['post_password'],
        $row['post_name'],
        $row['to_ping'],
        $row['pinged'],
        $row['post_modified'],
        $row['post_modified_gmt'],
        $row['post_content_filtered'],
        $row['post_parent'],
        $row['guid'],
        $row['menu_order'],
        $row['post_type'],
        $row['post_mime_type'],
        $row['comment_count']
    );
	$sql_insert = $wpdb->query($post_sql);
	//$wpdb->query($sql_insert);
    //$insert_post_res = mysqli_query($post_sql);
    $new_id = $wpdb->insert_id;


    // and the postmeta
    $sql_meta =  $wpdb->query("INSERT INTO wp_postmeta (post_id, meta_key, meta_value) SELECT " . $new_id . ", meta_key, meta_value FROM wp_postmeta_1 old WHERE old.post_id = " . $old_id);
	//$wpdb->query($sql_meta);
  

    

    // and then order items and order item meta
	$sql_order_item = sprintf("SELECT * FROM wp_woocommerce_order_items_1 WHERE order_id = %s", $old_id);
	$order_item_row = $wpdb->get_results($sql_order_item,ARRAY_A);	
    
    foreach($order_item_row as $item_row)
{
        $old_item_id = $item_row['order_item_id'];

        $item_sql1 = sprintf($item_sql_template,
            $item_row['order_item_name'],
            $item_row['order_item_type'],
            $new_id
        );
		$item_sql = $wpdb->query($item_sql1);
		//$wpdb->query($item_sql);
        $new_item_id = $wpdb->insert_id;
        $sql_meta = $wpdb->query("INSERT INTO wp_woocommerce_order_itemmeta (order_item_id, meta_key, meta_value) SELECT " . $new_item_id . ", meta_key, meta_value FROM wp_woocommerce_order_itemmeta_1 old WHERE old.order_item_id = " . $old_item_id);
        //$wpdb->query($sql_meta);

    }
 
} */ 


/*
global $wpdb;

$duplicate_titles = $wpdb->get_col("SELECT post_title FROM wp_posts WHERE `post_type` = 'shop_order' GROUP BY post_title HAVING COUNT(*) > 1");

foreach( $duplicate_titles as $title ) {
    $post_ids = $wpdb->get_col( $wpdb->prepare( "SELECT ID FROM wp_posts  WHERE post_title=%s", $title ) ); 
    // Iterate over the second ID with this post title till the last
    foreach( array_slice( $post_ids, 1 ) as $post_id ) {
        wp_delete_post( $post_id, true ); // Force delete this post
    }
}*/
?> 
		</div>
	</div>
</div>

<?php get_footer(); ?>
