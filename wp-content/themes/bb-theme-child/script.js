  var $ = jQuery;
    (function($) {
        $(function() {
            if ($('.open_sidebar').length <= 0) {
                $('.facetwp-template').before('<div class="open_sidebar"><a class="fl-button" href="#"><span class="fl-button-text">Filter By</span></a></div>');
            }
            if (typeof FWP !== 'undefined') {
                FWP.loading_handler = function() {
                    $(".facet_filters .close_sidebar").click(function(e) {
                        e.preventDefault();
                        $(".facet_filters").animate({ "left": -$(".facet_filters").outerWidth() - 20 }, 500);
                    });
                    $(".open_sidebar").click(function(e) {
                        e.preventDefault();
                        $(".facet_filters").css("left", -$(".facet_filters").outerWidth() - 20);
                        $(".facet_filters").animate({ "left": 0 }, 500);
                    });
                }
            }
        });
    })(jQuery);

$(document).ready(function() {
    var windowWidth = $(window).width();
        if (windowWidth <= 769) {
            if ($('.facet_filters .fl-html > .close_bar').length == 0) {
            $('.facet_filters .fl-html').prepend('<div class="close_bar"><a class="close close_sidebar" href="javascript:void(0)"><i class="fa fa-close">&nbsp;</i></a></div>');
        }
        if ($('.facet_filters .fl-html > .close_sidebar_button').length == 0) {
            $('.facet_filters .fl-html').append('<div class="close_sidebar_button"><a class="fl-button close_sidebar" href="#"><span class="fl-button-text">View Results</span></a></div>');
        }
    } 
})
