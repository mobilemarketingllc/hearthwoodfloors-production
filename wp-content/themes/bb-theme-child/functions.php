<?php

// Defines
define( 'FL_CHILD_THEME_DIR', get_stylesheet_directory() );
define( 'FL_CHILD_THEME_URL', get_stylesheet_directory_uri() );

// Classes
require_once 'classes/class-fl-child-theme.php';



// Actions
add_action( 'wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000 );

add_action( 'wp_enqueue_scripts', function(){
    wp_enqueue_script("Child-js",get_stylesheet_directory_uri()."/script.js","","",1);
});

function woocommerce_product_category_list( $args = array() ) {
  
  $terms = get_terms( 'product_cat', $args );
  ob_start();
  if ( $terms ) {
      ?> <ul class="woocommerce-categories"> <?php
      foreach ( $terms as $term ) {
        if($term->term_id == '127' || $term->term_id == '101' || $term->term_id == '99'){continue;}
          ?> <li class="woocommerce-product-category-page">
          <a href="<?php echo esc_url( get_term_link( $term ) );?>" class="<?php echo $term->slug.'-'.$term->term_id; ?> ">
          <?php 
            woocommerce_subcategory_thumbnail( $term );
          ?> <h2><?php echo $term->name; ?></h2>
            </a>
          </li> <?php
      }
     ?> </ul> <?php
  }
  return ob_get_clean();
}
add_shortcode( 'PRODUCT_CAT_LIST', 'woocommerce_product_category_list' );

// Creating Shortcode for Product Sorting
add_shortcode('wc_sorting','woocommerce_catalog_ordering');

add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );

function woo_remove_product_tabs( $tabs ) {

    unset( $tabs['description'] );      	// Remove the description tab
    unset( $tabs['reviews'] ); 			// Remove the reviews tab
     

    return $tabs;

}
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
add_action( 'woocommerce_single_product_summary', 'ta_the_content' );

function ta_the_content() {
        echo the_content();
}
// Creating Shortcode for Product Sorting
// add_shortcode('wc_sorting','woocommerce_catalog_ordering');

//Facet Title Hook
add_filter( 'facetwp_shortcode_html', function( $output, $atts ) {
  if ( isset( $atts['facet'] ) ) {       
      $output= '<div class="facet-wrap"><strong>'.$atts['title'].'</strong>'. $output .'</div>';
  }
  return $output;
}, 10, 2 );

add_filter( 'facetwp_per_page_options', function( $options ) {
  return array( 12,24,36,48,60 );
});

/* SKU Disable in PDP*/
add_filter( 'wc_product_sku_enabled', '__return_false' );

/* Filter : to hide price*/
add_filter( 'woocommerce_get_price_html', function( $price, $product ) {
if ( is_admin() ) return $price;
// Hide for these category slugs / IDs
$hide_for_categories = array( 'pen-kit' );
// Don’t show price when its in one of the categories
if ( !has_term( $hide_for_categories, 'product_cat', $product->get_id() ) ) {
return '';
}
return $price; // Return original price
}, 10, 2 );
add_filter( 'woocommerce_cart_item_price', '__return_false' );
add_filter( 'woocommerce_cart_item_subtotal', '__return_false' );

/* Change cart button position */

remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30);
add_action('woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 5);

// To change add to cart text on single product page
add_filter( 'woocommerce_product_single_add_to_cart_text', 'woocommerce_custom_single_add_to_cart_text' ); 
function woocommerce_custom_single_add_to_cart_text() {
    return __( 'ORDER NOW', 'woocommerce' ); 
}

/**
 * @snippet       Remove Zoom, Gallery @ Single Product Page
 * @how-to        Get CustomizeWoo.com FREE
 * @author        Rodolfo Melogli
 * @testedwith    WooCommerce 3.8
 * @donate $9     https://businessbloomer.com/bloomer-armada/
 */
  
add_action( 'wp', 'bbloomer_remove_zoom_lightbox_theme_support', 99 );
  
function bbloomer_remove_zoom_lightbox_theme_support() { 
   remove_theme_support( 'wc-product-gallery-zoom' );
  // remove_theme_support( 'wc-product-gallery-lightbox' );
  // remove_theme_support( 'wc-product-gallery-slider' );
}